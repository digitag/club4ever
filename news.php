<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body>
    <?php include('layout/header-login.php'); ?>

        <div class="container">
            <h1>News</h1>

            <div class="news red">
                <div class="row">
                    <div class="col-xs-11">
                        <h2>Titolo della news interessante</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscin elit. </p>
                    </div>
                        <a href="dettaglio-news.php"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
                </div>
            </div>
            <div class="news blue">
                <div class="row">
                    <div class="col-xs-11">
                        <h2>Titolo della news molto molto interessante</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscin elit. </p>
                    </div>
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                </div>
            </div>
            <div class="news green">
                <div class="row">
                    <div class="col-xs-11">
                        <h2>Titolo della news molto molto interessante</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscin elit. </p>
                    </div>
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                </div>
            </div>
            <div class="news orange">
                <div class="row">
                    <div class="col-xs-11">
                        <h2>Titolo della news molto molto interessante</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscin elit. </p>
                    </div>
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                </div>
            </div>

        </div>

        <?php include('layout/footer.php'); ?>

</body>

</html>
