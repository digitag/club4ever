<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body>
    <?php include('layout/header.php'); ?>
        <div class="container text-center">
            <h1 class="title">Perché <span>club</span>4<span>ever</span></h1>
            <ul>
                <li>a chi desidera un’ indipendenza economica</li>
                <li>attraverso un percorso di carriera chiaro</li>
                <li>con guadagni certi e misurabili</li>
                <li>con flessibilità di orari e senza stress</li>
            </ul>
        </div>
        <div id="carriera" class="bg-green container-fluid">
            <div class="container">
                <h1 class="title">La tua carriera con noi</h1>
                <div class="row">
                    <div class="col-sm-3 col-md-2 hidden-xs">
                        <p class="manager">Manager</p>
                        <p class="assistant-manager">Assistant
                            <br> Manager</p>
                        <p class="supervisor">Supervisor</p>
                        <p class="assistant-supervisor">Assistant
                            <br> Supervisor</p>
                        <p class="me">Nuovo
                            <br> incaricato</p>
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li><strong>30 persone - oltre 1.600 euro</strong>
                                <br>48% di sconto su miei acquisti</li>
                            <li><strong>14 persone - oltre 1.000 euro</strong>
                                <br>43% di sconto sui miei acquisti</li>
                            <li><strong>6 persone - oltre 600 euro</strong>
                                <br>38% di sconto sui miei acquisti
                            </li>
                            <li><strong>2 persone - oltre 200 euro</strong>
                                <br>48% di sconto sui miei acquisti</li>
                            <li><strong>Me</strong>
                                <br>15% di sconto sui miei acquisti</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container text-center">
            <p class="title">
                Assistenza e tutela
            </p>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <ul>
                        <li>regolamentato dalla legge 173/2005</li>
                        <li>tutelato da Avedisco</li>
                        <li>un’ attività che i tuoi figli potranno ereditare</li>
                        <li>dal 15% al 48% per gli ordini dei tuoi clienti</li>
                        <li>ulteriori bonus per te sui risultati del tuo team</li>
                        <li>sconti dal 15% al 48% suoi tuoi ordini personali</li>
                        <li>pagamenti regolari a norma di legge</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-green container-fluid text-center">
            <div class="container">
                <h1 class="title">Un'azienda che offre una tassazione agevolata </h1>
                <ul>
                    <li>23% sul 78% di imponibile = 17,64%</li>

                    <li>esente da dichiarazione dei redditi</li>

                    <li>redditi non cumulabili ad altri redditi esistenti</li>

                    <li>ritenute d’imposta alla fonte, no burocrazia
                    </li>
                </ul>
            </div>
        </div>
            <div class="bg-red-quotation container-fluid text-center">
                <p class="title">Comincia ora, cambia il tuo futuro</p>
    </div>
        <?php include('layout/footer.php'); ?>

</body>

</html>
