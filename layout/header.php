<nav class="navbar navbar-default">
  <div class="container-fluid">
            <a id="logo" href="index.php"></a>

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Chi siamo</a></li>
        <li><a href="#">Contatti</a></li>
        <li class="hidden-xs login-sm"><a class="white" href="login.php"><span>Fai già parte del club?</span><br>
            ACCEDI O REGISTRATI</a></li>
        <li><a class="visible-xs" href="login.php">LOGIN E REGISTRAZIONE</a></li>

      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>