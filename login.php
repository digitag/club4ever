<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body class="text-center">
    <?php include('layout/header.php'); ?>

        <div class="container">
            <form>
                <div class="container-form">
                    <h1>Login</h1>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail1">FLP Id</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="FLP Id">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                </div>
                <button onclick="window.location='news.php'" class="btn btn-default">ENTRA</button>
            </form>

            <a href="#" class="link">Password dimenticata?</a>

            <a href="registrati.php" class="btn btn-default">Registrati</a>

        </div>


        <?php include('layout/footer.php'); ?>

</body>

</html>