<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body>
    <?php include('layout/header.php'); ?>
        <div class="container padding-bottom-60">
            <h1 class="title">Chi è <span>forever living</span></h1>
            <h2 class="text-center">Una solita azienda internazionale</h2>
            <p>Forever Living Products viene fondata nel 1978 da Rex Maughan, un uomo con una grande vision che, scoperte le molte virtù terapeutiche dell’Aloe Vera, decide di creare un’azienda per la sua coltivazione e commercializzazione. Da allora l’azienda è cresciuta in modo costante e oggi è presente in 158 Paesi nel mondo con vendite che rasentano i 3 miliardi di dollari. </p>
            <p>La sua fortuna è stata di aver scelto un prodotto di sicuro successo in un ambito, quello della prevenzione primaria e della bellezza, che non ha subito flessioni anche nei momenti di crisi; inoltre ha scelto un sistema distributivo, quello del Network Marketing, che ha avuto un grande successo dando la possibilità ai quasi dieci milioni di distributori oggi affiliati a Forever di ottenere ottimi risultati imprenditoriali.</p>
            <p>Forever ha svariate affiliate tramite cui segue l’intero processo produttivo: dalla coltivazione dell’aloe vera in piantagioni di sua proprietà secondo metodi biologici, alla raccolta, dalla lavorazione -secondo un processo brevettato – al confezionamento e alla distribuzione. Parallelamente a queste attività produttive, ha sviluppato la rete distributiva con sedi di proprietà in tutto il mondo dove vengono organizzati corsi di formazione specifici per ogni Paese. Forever è proprietaria anche di numerosi resort, soprattutto in America, e sostiene un importante programma di beneficenza.
            </p>
        </div>
        <div id="prodotti-aloe" class="bg-green container-fluid">
            <div class="container">
                <h1 class="title">Prodotti a base di aloe</h1>
                <p>Perché prodotti a base di Aloe </p>

                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>Aiuta l’organismo a depurarsi dalle tossine e ridurre gli stati infiammatori</li>
                            <li>È un supplemento nutrizionale che fornisce aminoacidi, vitamine e minerali</li>
                            <li>Sviluppa le difese immunitarie dell’organismo</li>
                            <li>È una bevanda con azione emolliente e lenitiva del sistema digerente </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>


        <?php include('layout/footer.php'); ?>

</body>

</html>
