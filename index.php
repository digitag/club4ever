<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body>
    <?php include('layout/header.php'); ?>
        <div id="home" class="contenier-fluid">
            <p class="title">We believe in <span>us</span></p>
            <button type="button" class="btn btn-default btn-lg button-down">
              <span class="glyphicon glyphicon-menu-down " aria-hidden="true"></span>
            </button>
        </div>

            <div class="container">
                <h1 class="title">Perché <span>club</span>4<span>ever</span></h1>
                <ul>
                    <li>Per condividere il tuo percorso con persone esperte che possono sostenerti</li>
                    <li>Per partecipare alla formazione ed essere aggiornati su tutti gli eventi in programma</li>
                    <li>Per condividere un modello collaudato nato dall’esperienza di oltre 1.200 membri</li>
                    <li>Per accedere al sito e all’app Club4Ever. Strumenti che ti permettono di essere sempre aggiornato sulle ultime novità e che velocizzano ulteriormente la tua carriera </li>
                </ul>
                <a class="bottone" href="perche-club4ever.php">Scopri di più</a>
            </div>
            <div class="container-fluid bg-red">
                <div class="container">
                    <p class="title">
                        Perché forever living
                    </p>
                    <ul>
                        <li>Oltre 10 milioni di incaricati nel mondo</li>
                        <li>Presente in più di 150 paesi</li>
                        <li>Fondata nel 1978</li>
                        <li>Più di 2 miliardi di dollari il fatturato mondiale</li>
                        <li>Più di 50 milioni di euro il fatturato italiano</li>
                        <li>Accreditata in tutto il mondo per la qualità dei suoi prodotti</li>
                    </ul>
                <a class="bottone" href="perche-forever-living.php">Scopri di più</a>
                </div>
            </div>
        <?php include('layout/footer.php'); ?>

</body>

</html>
