<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body class="text-center">
    <?php include('layout/header-login.php'); ?>

        <div class="container">

            <h1>Invia documenti</h1>
            <div class="row text-left" id="invio-docs">
                <div class="col-xs-2 red"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></div>
                <div class="col-xs-10 red-lighter"><a href="#">Descizione del PDF</a> <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></div>
                
                <div class="col-xs-2 blue"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></div>
                <div class="col-xs-10 blue-lighter"><a href="#">Descizione del link</a> <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></div>
                
                <div class="col-xs-2 green"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span></div>
                <div class="col-xs-10 green-lighter"><a href="#">Descizione del Video</a> <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></div>
                
                <div class="col-xs-2 orange"><span class="glyphicon glyphicon-list-alt
                    " aria-hidden="true"></span></div>
                <div class="col-xs-10 orange-lighter"><a href="#">Descizione del documento</a> <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></div>
                
            </div>
               
                        <a class="btn btn-default" href="categorie-documenti.php"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Torna alle categorie</a>

            
            </div>
        </div>

        <?php include('layout/footer.php'); ?>

</body>

</html>