<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body class="text-center">
    <?php include('layout/header-login.php'); ?>

        <div class="container">

            <h1>Invia documenti</h1>
            <h2>Categorie documenti</h2>
            <div class="row text-left" id="invio-docs">
                <div class="col-xs-2 gray"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></div>
                <div class="col-xs-10 gray-light"><a href="invia-documenti.php">PRIMI PASSI <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></a></div>
                
                <div class="col-xs-2 gray"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></div>
                <div class="col-xs-10 gray-light"><a href="invia-documenti.php">COSMESI <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></a></div>
                
                <div class="col-xs-2 gray"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span></div>
                <div class="col-xs-10 gray-light"><a href="invia-documenti.php">INTEGRATORI <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></a></div>
                
                <div class="col-xs-2 gray"><span class="glyphicon glyphicon-list-alt
                    " aria-hidden="true"></span></div>
                <div class="col-xs-10 gray-light"><a href="invia-documenti.php">OILS <span class="glyphicon glyphicon-menu-right pull-right" aria-hidden="true"></span></a></div>
                
            </div>
               
            </div>
        </div>

        <?php include('layout/footer.php'); ?>

</body>

</html>

