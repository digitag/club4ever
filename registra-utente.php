<!DOCTYPE html>
<html>

<head>
    <title>Club4ever</title>
    <?php include('layout/head.php'); ?>
</head>

<body class="text-center">
    <?php include('layout/header-login.php'); ?>

        <div class="container">
            <form>
                <div class="container-form">
                    <h1>Registra un nuovo utente</h1>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputNome">Nome</label>
                        <input type="text" class="form-control" id="exampleInputNome" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputCognome">Cognome</label>
                        <input type="text" class="form-control" id="exampleInputCognome" placeholder="Cognome">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail">Codice fiscale</label>
                        <input type="text" class="form-control" id="exampleInputEmail" placeholder="Codice fiscale">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email">
                    </div>
                </div>
                <button class="btn btn-default">INVIA</button>
            </form>
    
    </div>


        <?php include('layout/footer.php'); ?>

</body>

</html>
